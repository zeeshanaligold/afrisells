import React from 'react';
import {ScrollView, View, Image} from 'react-native';
import {Header, Text, ThemedView} from 'src/components';

import Container from 'src/containers/Container';
import {TextHeader, IconHeader, CartIcon} from 'src/containers/HeaderComponent';

import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config/fonts';

export default class ContactScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    const {
      screenProps: {t, theme},
    } = this.props;
    const image =
      theme.key === 'dark'
        ? require('src/assets/images/searchDark.png')
        : require('src/assets/images/searchLight.png');

    return (
      <ThemedView style={styles.full}>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={<TextHeader title={t('common:text_about')} />}
          rightComponent={<CartIcon />}
        />
        <ScrollView>
          <Container>
            <Text h2 medium>
              {t('common:text_about')}
            </Text>
            <Text style={styles.description} colorSecondary>
              {t('profile:text_about_description')}
            </Text>
            <Text h3 medium>
              Show your loved ones you care
            </Text>
            <Text style={styles.description} colorSecondary>
              With reliable 24-hour delivery from Checkout, you can send Grocery
              and more to your family members back.
            </Text>
            <Text h3 medium>
              Purchase and Delivery of Food
            </Text>
            <Text style={styles.description} colorSecondary>
              Afrisells.com has taken steps to ease the process of sending
              grocery and others to family’s members back home. With the
              development of the Online Portal that allows food to be sent to
              families in Sudan, Kool Communications LLC & Kool Communications
              RAK setup offices in USA & UAE to make the process more efficient
              and guarantee reliability. Yes, when you send. They will receive.
            </Text>
            <Text style={styles.description} colorSecondary>
              In the comfort of your home, you can easily buy and send Grocery,
              Electricity & Phone recharge, Electronics to friends and families
              in Sudan.
            </Text>
          </Container>
        </ScrollView>
      </ThemedView>
    );
  }
}

const styles = {
  full: {
    flex: 1,
  },
  textName: color => ({
    color: color,
    marginBottom: margin.big,
  }),
  viewImage: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: margin.big,
  },
  description: {
    textAlign: 'justify',
    marginVertical: margin.base,
    lineHeight: lineHeights.h4,
  },
};
