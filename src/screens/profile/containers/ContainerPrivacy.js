import React from 'react';
import {useTranslation} from 'react-i18next';
import {ScrollView, StyleSheet} from 'react-native';
import {Text} from 'src/components';
import Container from 'src/containers/Container';
import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config';

const ContainerPrivacy = () => {
  const {t} = useTranslation();

  return (
    <ScrollView>
      <Container>
        <Text h2 medium style={styles.title}>
          {t('common:text_privacy')}
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 1 – WHAT DO WE DO WITH YOUR INFORMATION?
        </Text>
        <Text colorSecondary style={styles.description}>
          When you purchase something from our store, as part of the buying and
          selling process, we collect the personal information you give us such
          as your name, address and email address.
        </Text>
        <Text colorSecondary style={styles.description}>
          When you browse our store, we also automatically receive your
          computer’s internet protocol (IP) address in order to provide us with
          information that helps us learn about your browser and operating
          system.
        </Text>
        <Text colorSecondary style={styles.description}>
          Email marketing (if applicable): With your permission, we may send you
          emails about our store, new products and other updates.
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 2 – CONSENT
        </Text>
        <Text h5 style={styles.titleList}>
          How do you get my consent?
        </Text>
        <Text colorSecondary style={styles.description}>
          When you provide us with personal information to complete a
          transaction, verify your credit card, place an order, arrange for a
          delivery or return a purchase, we imply that you consent to our
          collecting it and using it for that specific reason only.
        </Text>
        <Text colorSecondary style={styles.description}>
          If we ask for your personal information for a secondary reason, like
          marketing, we will either ask you directly for your expressed consent,
          or provide you with an opportunity to say no.
        </Text>
        <Text h5 style={styles.titleList}>
          How do I withdraw my consent?
        </Text>
        <Text colorSecondary style={styles.description}>
          If after you opt-in, you change your mind, you may withdraw your
          consent for us to contact you, for the continued collection, use or
          disclosure of your information, at any time, by contacting us at
          info@afrisells.com or mailing us at: MTM Solutions LLC 5671 COLUMBIA
          PIKE STE 201, FALLS CHURCH, VA 22041
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 3 – DISCLOSURE
        </Text>
        <Text colorSecondary style={styles.description}>
          We may disclose your personal information if we are required by law to
          do so or if you violate our Terms of Service.
        </Text>
        <Text h4 style={styles.titleList}>
          Payment:
        </Text>
        <Text colorSecondary style={styles.description}>
          If you choose a direct payment gateway to complete your purchase, then
          we store your credit card data. It is encrypted through the Payment
          Card Industry Data Security Standard (PCI-DSS). Your purchase
          transaction data is stored only as long as is necessary to complete
          your purchase transaction. After that is complete, your purchase
          transaction information is deleted.
        </Text>
        <Text colorSecondary style={styles.description}>
          All direct payment gateways adhere to the standards set by PCI-DSS as
          managed by the PCI Security Standards Council, which is a joint effort
          of brands like Visa, MasterCard, American Express and Discover.
        </Text>
        <Text colorSecondary style={styles.description}>
          PCI-DSS requirements help ensure the secure handling of credit card
          information by our store and its service providers.
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 5 – THIRD-PARTY SERVICES
        </Text>
        <Text colorSecondary style={styles.description}>
          In general, the third-party providers used by us will only collect,
          use and disclose your information to the extent necessary to allow
          them to perform the services they provide to us.
        </Text>
        <Text colorSecondary style={styles.description}>
          However, certain third-party service providers, such as payment
          gateways and other payment transaction processors, have their own
          privacy policies in respect to the information we are required to
          provide to them for your purchase-related transactions.
        </Text>
        <Text colorSecondary style={styles.description}>
          For these providers, we recommend that you read their privacy policies
          so you can understand the manner in which your personal information
          will be handled by these providers.
        </Text>
        <Text colorSecondary style={styles.description}>
          In particular, remember that certain providers may be located in or
          have facilities that are located in a different jurisdiction than
          either you or us. So if you elect to proceed with a transaction that
          involves the services of a third-party service provider, then your
          information may become subject to the laws of the jurisdiction(s) in
          which that service provider or its facilities are located.
        </Text>
        <Text colorSecondary style={styles.description}>
          As an example, if you are located in Canada and your transaction is
          processed by a payment gateway located in the United States, then your
          personal information used in completing that transaction may be
          subject to disclosure under United States legislation, including the
          Patriot Act.
        </Text>
        <Text colorSecondary style={styles.description}>
          Once you leave our store’s website or are redirected to a third-party
          website or application, you are no longer governed by this Privacy
          Policy or our website’s Terms of Service.
        </Text>
        <Text h4 style={styles.titleList}>
          Links
        </Text>
        <Text colorSecondary style={styles.description}>
          When you click on links on our store, they may direct you away from
          our site. We are not responsible for the privacy practices of other
          sites and encourage you to read their privacy statements.
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 6 – SECURITY
        </Text>
        <Text colorSecondary style={styles.description}>
          To protect your personal information, we take reasonable precautions
          and follow industry best practices to make sure it is not
          inappropriately lost, misused, accessed, disclosed, altered or
          destroyed.
        </Text>
        <Text colorSecondary style={styles.description}>
          If you provide us with your credit card information, the information
          is encrypted using secure socket layer technology (SSL) and stored
          with a AES-256 encryption. Although no method of transmission over the
          Internet or electronic storage is 100% secure, we follow all PCI-DSS
          requirements and implement additional generally accepted industry
          standards.
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 7 – AGE OF CONSENT
        </Text>
        <Text colorSecondary style={styles.description}>
          By using this site, you represent that you are at least the age of
          majority in your state or province of residence, or that you are the
          age of majority in your state or province of residence and you have
          given us your consent to allow any of your minor dependents to use
          this site.
        </Text>
        <Text h4 style={styles.titleList}>
          SECTION 8 – CHANGES TO THIS PRIVACY POLICY
        </Text>
        <Text colorSecondary style={styles.description}>
          We reserve the right to modify this privacy policy at any time, so
          please review it frequently. Changes and clarifications will take
          effect immediately upon their posting on the website. If we make
          material changes to this policy, we will notify you here that it has
          been updated, so that you are aware of what information we collect,
          how we use it, and under what circumstances, if any, we use and/or
          disclose it.
        </Text>
        <Text colorSecondary style={styles.description}>
          If our store is acquired or merged with another company, your
          information may be transferred to the new owners so that we may
          continue to sell products to you.
        </Text>
        <Text h4 style={styles.titleList}>
          QUESTIONS AND CONTACT INFORMATION
        </Text>
        <Text colorSecondary style={styles.description}>
          If you would like to: access, correct, amend or delete any personal
          information we have about you, register a complaint, or simply want
          more information contact our Privacy Compliance Officer at
          info@afrisells.com or by mail at MTM Solutions LLC
        </Text>
        <Text h5 style={styles.titleList}>
          [Re: Privacy Compliance Officer]
        </Text>
        <Text h5 style={styles.titleList}>
          [5671 COLUMBIA PIKE STE 201, FALLS CHURCH, VA 22041]
        </Text>
      </Container>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  title: {
    marginBottom: margin.big,
  },
  titleList: {
    marginBottom: margin.base + 4,
  },
  description: {
    marginBottom: 10,
    textAlign: 'justify',
    lineHeight: lineHeights.h4,
  },
});

export default ContainerPrivacy;
